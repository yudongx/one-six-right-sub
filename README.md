# One Six Right 字幕

目标：精校精译的中英文双语字幕

使用软件Aegisub和Arctime Pro。

一些教程：

- https://zhuanlan.zhihu.com/p/34106690
- https://aegi.vmoe.info/docs/3.2/Tutorials/
- https://www.apporid.com/news/yyjc/13774.html
- https://metricsubs.org/aegisub%E6%95%99%E7%A8%8B%E7%B3%BB%E5%88%97-%E7%B4%A2%E5%BC%95/
